const createCounter = () => {
    let count = 0;
    return {
        increment() {
            count++;
        },
        decrement() {
            count--;
        },
        get() {
            return count;
        }
    }
};

const counter = createCounter();

counter.increment();
counter.decrement();
counter.decrement();

//console.log(counter.get());

const createAdder = (a) => {
    return (b) => {
        return a + b;
    }
}

const addTen = createAdder(10);

/////console.log(addTen(-2));
//console.log(addTen(20));

const add100 = createAdder(100);
//console.log(add100(-90))

// Tripper Challenge 
// 1. Create createTipper which takes in the base tip (.15 for 15% tip).
// 2. Set it up to return a function that takes in the bill amount. 
// 3. Call createTipper to generate a few functions for different percentages.
// 4. Use the generated functions to calculate tips and print them.

const createTipper = (tip) => {
    return (bill) => {
        return bill * tip;
    }
}

const tip15Percent = createTipper(.15);
const tip20Percent = createTipper(.20);
const tip25Percent = createTipper(.25);
console.log(`The total amount due for this bill, including tip, is: ${tip15Percent(10)}`);
console.log(tip20Percent(50));
console.log(tip25Percent(80));